// JavaScript Document
var printerObj = new Printer();
var dgsObj = new DGS();

function doPrintReception(type,str)
{
	if( printerObj.device.getReceiptPrinter()==undefined)
		 return;
 //   printHeader(type);
    printBody(str,type);
  //  printTail(type);
   
}

/*
 * type：类型1：商户，2：客户,strtime:交易时间
 */
function printHeader(type)
{
    var str1 = "";

    if (type == 1)
    {
        str1 = "             欢迎光临\n";
    }
    else if (type == 2)
    {
        str1 = "             日结清单\n";
    }
   
    printerObj.device.setFont(1);
    printerObj.device.printString(str1);

    var str2 = "";
    var dgsObj = new DGS();
    //str2+="\n"+"终端号："+dgsObj.service.get("ini_terId")+"\n";
    if (type == 1)
    str2 += "             售货清单\n";
   
    printerObj.device.setFont(0);
    if (type == 1)
    printerObj.device.printString(str2);
}

//每次最多打印pCount行
function printBody(bodyStr,type)
{
	if(type==1)
    printerObj.device.setFont(0);
	else if(type==2)
	    printerObj.device.setFont(1);
	
    var pCount = 20;
    var strArray = bodyStr.split("\n");
    var i = 0;
    var printStr = "";
    while (i < strArray.length - 1)
    {

        if (i > 0 && i % 20 == 0)
        {
            printStr += strArray[i];
            printerObj.device.printString(printStr);
            printStr = "";
        }
        else
        {
            printStr += strArray[i] + "\n";
        }
        i++;
    }
    printStr += strArray[strArray.length - 1];
	printStr+="\n\n\n\n\n";
    if (printStr.length > 0)
    {
        printerObj.device.printString(printStr);
    }

}

function printTail(type)
{
    //printerObj.device.setBold(0);
    //printerObj.device.setFont(0);
    var tailStr = "";
    if (type == 1)
    tailStr += " 请保留电脑小票作为退换货凭证" + "\n\n\n";
    else if (type == 2)
    	tailStr += "\n";
  //  if (type == 1)
   // {
     //   tailStr += "客户签名:\n\n\n\n\n=============================";
    //}
    printerObj.device.printString(tailStr);
    printerObj.device.goPaper(5);
    //printerObj.device.cut(1);

}
function makeSpace(orgStr)
{
    var str = orgStr.split("\n");
    var rStr = "";
    for (var i = 0; i < str.length; i++)
    {
        rStr += str[i] + "\n";
    }
    return rStr;
}



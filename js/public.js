// JavaScript Document
var Extend = function(destination, source) {  
    for (var property in source) {  
        destination[property] = source[property];  
    }  
}  

var Class = {  
    create: function() {  
        return function() { this.Init.apply(this, arguments); }  
    }  
}  
var $ = function (id) {  
    return "string" == typeof id ? document.getElementById(id) : id;  
};

function showmsg(parentdiv,controlname,tips)
{

	if($('msgdiv')) return;
	newDiv = document.createElement("div");
	newDiv.setAttribute('id','msgdiv');
	newDiv.style.textAlign="center";
	$(parentdiv).appendChild(newDiv);
	newDiv.innerHTML = "<span id='messagediv'>"+tips+"</span>";
	newDiv.style.position="absolute";
	newDiv.style.zIndex = "2";
	
	var m = $(controlname);

	var left = m.offsetLeft;
	var top = m.offsetTop;
	
	while(m.offsetParent.tagName.toUpperCase()!="DIV"&&m.offsetParent.tagName.toUpperCase()!="BODY")
	{
		
	 left += m.offsetParent.offsetLeft;
	 top+=m.offsetParent.offsetTop;
	 m = m.offsetParent;
	}
	
    
	top+=parseInt($(controlname).offsetHeight);
    
    top+=5;
	
	newDiv.style.left = left+"px";
	newDiv.style.top = top+"px";
	
	//newDiv.style.top = $(controlname).offsetTop+$(controlname).offsetHeight;
	
	
}


function sAlert(newDiv,newstyle,tips,type)
{

var iWidth = 1024;
var iHeight =600;

this.bgObj=document.createElement("div");
bgObj.setAttribute('id','bgDiv');
bgObj.style.position="absolute";
bgObj.style.top="0";
bgObj.style.background="#c0c0c0";
bgObj.style.filter="progid:DXImageTransform.Microsoft.Alpha(style=3,opacity=25,finishOpacity=75)";
bgObj.style.opacity="0.6";
bgObj.style.left="0";
bgObj.style.overflow = "hidden";
bgObj.style.width=iWidth + "px";
bgObj.style.height=iHeight + "px";
bgObj.style.zIndex = "1";
document.body.appendChild(bgObj);
if(!newDiv)
{
	newDiv = document.createElement("div");
	newDiv.setAttribute('id','tipdiv');
	newDiv.style.textAlign="center";

	if(typeof(type)=="undefined")
	newDiv.innerHTML = "<br><br><p >"+tips+"</p><br><input type='button' value='确定' onclick='closeself(\""+bgObj.id+"\",\""+newDiv.id+"\");'>"
	
	else
	{
	newDiv.innerHTML = "<br><br><p>"+tips+"</p>";
	window.setTimeout("closeself(\""+bgObj.id+"\",\""+newDiv.id+"\")", 1000)
	}
	document.body.appendChild(newDiv);
}
else
{
	
	var obj1 = newDiv.getElementsByTagName("div");
	
    for (var i=0; i<obj1.length; i++)
    {
		newDiv.removeChild(obj1[i]);
    }
		
}
 
newDiv.style.opacity="60";
newDiv.style.position="absolute";
newDiv.style.zIndex = "2";
newDiv.style.display = "block"; 
newDiv.style.overflow = "hidden";

newstyle = newstyle||{width:300,height:300,left:250,top:90};
if(newstyle)
{
  newDiv.style.width = newstyle.width+"px";//"300px";
  newDiv.style.height = newstyle.height+"px";//"300px";
  newDiv.style.left = newstyle.left+"px";//"250px";
  newDiv.style.top = newstyle.top+"px";//"90px";
  if(typeof(newstyle.background)=="undefined")
     newDiv.style.background = "#c0c0c0";
  else	 
  newDiv.style.background = newstyle.background;
}

}   

function tick() { 
var hours, minutes, seconds, xfile; 
var intHours, intMinutes, intSeconds; 
var today, theday; 
today = new Date(); 
function initArray(){ 
this.length=initArray.arguments.length; 
for(var i=0;i<this.length;i++) 
this[i+1]=initArray.arguments[i] } 
var d=new initArray( 
"星期日", 
"星期一", 
"星期二", 
"星期三", 
"星期四", 
"星期五", 
"星期六"); 
theday = today.getFullYear()+"年" + [today.getMonth()+1]+"月" +today.getDate() +"日 "+ d[today.getDay()+1]; 
intHours = today.getHours(); 
intMinutes = today.getMinutes(); 
intSeconds = today.getSeconds(); 
if (intHours < 10) { 
hours = "0"+intHours+":"; 

} 
else {
hours = intHours + ":"; 
}
if (intMinutes < 10) { 
minutes = "0"+intMinutes+":"; 
} else { 
minutes = intMinutes+":"; 
} 
if (intSeconds < 10) { 
seconds = "0"+intSeconds+" "; 
} else { 
seconds = intSeconds+" "; 
} 

timeString = theday+" "+hours+minutes+seconds; 
return timeString;

} 

